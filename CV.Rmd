---
name: Felipe Menino Carlos
date: "`r format(Sys.time(), '%B, %Y')`"
address: "Avenida Dr. Adhemar de Barros, 727, Ap. 51, 12245-010, Jardim São Dimas"
phone: +55 12982104764
position: "Birth Date: 22 September 1997"
orcid: 0000-0002-3334-4315
email: felipe.carlos@fatec.sp.gov.br
github: M3nin0
headcolor: 414141
output: vitae::awesomecv
---

```{r setup, include=FALSE}
library(tidyverse)
library(vitae)
knitr::opts_chunk$set(echo = FALSE)
```

# Summary

Undergrad in  Analysis and Systems Development at the Fatec 'Jessen Vidal' (2019) dedicated to the following topics: data mining, image processing, artificial intelligence

- ORCID: 0000-0002-3334-4315

# Education
```{r}
tribble(
  ~ degree, ~ uni, ~ loc, ~ dates, ~ details,
  "B.S in Analysis and Systems Development", "Fatec 'Jessen Vidal'", "São José dos Campos, SP, Brazil", "2016 - 2019", "ICan.js: Assistive web resources using deep learning techniques (in Portuguese, with English abstract). Supervisor: Giuliano Araujo Bertoti"
) %>% 
  detailed_entries(degree, dates, uni, loc, details)
```

# Publications in indexed journals
```{r}
tribble(
  ~ details, ~ loc, ~ dates,
    "LOBO, F.L.; SOUZA FILHO, PEDRO W.M.; NOVO, E.M.L.M.; CARLOS, F.M ; BARBOSA, C.C.F.. Mapping Mining Areas in the Brazilian Amazon Using MSI/Sentinel-2 Imagery (2017) Remote Sensing , v. 10, p. 1178, 2018.", "Remote Sensing", "2018") %>% 
  detailed_entries(details, loc, dates)
```

# Main Publications presented during conferences

```{r}
tribble(
  ~ details, ~ loc, ~ dates,
    "FLORES JUNIOR, R. ; BARBOSA, C. C. F. ; LOBO, F. L. ; NOVO, E. M. L. M. ; MACIEL, D. A. ; CARLOS, F. M. . ASSESMENT OF SATELLITE DERIVED CHLOROPHYL-A EMPIRICAL ALGORITHMS FOR TURBID WATER OF THE AMAZON FLOODPLAIN LAKES. In: AmeriGEOSS Week 2018, 2018, São José dos Campos. Anais do AmeriGEOSS Week 2018, 2018.", "AmeriGEOSS Week 2018", "2018",
  "SILVA, R. B. L. ; MONTANHER, O. C. ; NOVO, E. M. L. M. ; BARBOSA, C. C. F. ; MACIEL, D. A. ; CARLOS, F. M. . RELAÇÃO ENTRE O TOTAL DE SÓLIDOS SUSPENSOS EM CORPOS HÍDRICOS DO ALTO RIO PARANÁ E IMAGENS MSI/SENTINEL-2: ESTUDO PRELIMINAR. In: XIX Simpósio Brasileiro de Sensoriamento Remoto, 2019, Santos. Anais do XIX Simpósio Brasileiro de Sensoriamento Remoto, 2019.", "XIX Simpósio Brasileiro de Sensoriamento Remoto", "2019",
  "CARLOS, F. M.; MARTINS, V. S; BARBOSA, C. C. F. . SISTEMA SEMIAUTOMÁTICO DE CORREÇÃO ATMOSFÉRICA PARA MULTI-SENSORES ORBITAIS. In: XIX Simpósio Brasileiro de Sensoriamento Remoto, 2019, Santos. Anais do XIX Simpósio Brasileiro de Sensoriamento Remoto, 2019.", "XIX Simpósio Brasileiro de Sensoriamento Remoto", "",
  "MACIEL, D. A.; FLORES JUNIOR, R. ; CAIRO, C. T. ; CARVALHO, L. A. S. ; LOBO, F. L. ; CARLOS, F. M. ; MARTINS, V. S. ; BARBOSA, C. C. F. ; NOVO, E. M. L. M. . MODELING SUSPENDED SEDIMENTS IN AMAZON FLOODPLAINS USING ORBITAL MODERATE RESOLUTION SENSORS. In: XIX Simpósio Brasileiro de Sensoriamento Remoto, 2019, Santos. Anais do XIX Simpósio Brasileiro de Sensoriamento Remoto, 2019.", "XIX Simpósio Brasileiro de Sensoriamento Remoto", "",
  "CAIRO, C. T. ; MACIEL, D. A. ; FLORES JUNIOR, R. ; CARLOS, F. M. ; BARBOSA, C. C. F. ; LOBO, F. L. ; NOVO, E. M. L. M. . ESTIMATIVA DA CONCENTRAÇÃO DA CLOROFILA-a NO RESERVATÓRIO DE IBITINGA/SP ? RESULTADOS PRELIMINARES. In: XIX Simpósio Brasileiro de Sensoriamento Remoto, 2019, Santos. Anais do XIX Simpósio Brasileiro de Sensoriamento Remoto,, 2019.", "XIX Simpósio Brasileiro de Sensoriamento Remoto", "",
  "FLORES JUNIOR, R. ; MACIEL, D. A. ; CAIRO, C. T. ; CARLOS, F. M. ; LOBO, F. L. ; CARVALHO, L. A. S. ; NOVO, E. M. L. M. ; BARBOSA, C. C. F. . ASSESSMENT OF SATELLITE ALGORITHMS FOR DERIVING CHLOROPHYLL-A FROM TURBID WATERS OF AMAZON FLOODPLAIN LAKES. In: XIX Simpósio Brasileiro de Sensoriamento Remoto, 2019, Santos. Anais do XIX Simpósio Brasileiro de Sensoriamento Remoto, 2019.", "XIX Simpósio Brasileiro de Sensoriamento Remoto", ""
) %>% 
  detailed_entries(details, loc, dates)
```

\newpage

# Experience
```{r}
tribble(
  ~ role, ~ company, ~ loc, ~ dates, ~ details,
  "Associate Student", "National Institute for Space Research", "São José dos Campos, SP, Brazil", "2017 - Present", "Development of routines for image processing"
) %>% 
  detailed_entries(role, dates, company, loc, details)
```

# Additional Relevant Skills

- Advanced programming skills (Python);
- Intermediate programming skills (R, Matlab, Node.js, Java).

<!-- \newpage -->

<!-- # Relevant Skills -->
<!-- ```{r} -->
<!-- cvskill("Data Analysis", c("Visualisation", "Data Cleaning", "Forecasting", "Modelling", "Communication", "Inference", "Machine Learning", "Web Scraping")) -->
<!-- cvskill("Programming", c("R", "Python", "SQL", "Java", "C", "C++", "HTML/CSS", "TeX", "MATLAB")) -->
<!-- ``` -->

<!-- # Awards & Achievements
## Awards
```{r}
tribble(
  ~ award, ~ from, ~ year,
  "Commerce Dean's Honour", "Monash", "2017",
  "Commerce Dean's Commendation", "Monash", "2016",
  "Science Dean’s List", "Monash", "2014-2016",
  "International Institute of Forecasters Award", "IIF", "2014",
  "Rotary Youth Leadership Award", "Rotary", "2013"
) %>%
  brief_entries(award, year, from)
```

## Scholarships
```{r}
tribble(
  ~ scholarship, ~ from, ~ year,
  "Econometrics Honours Memorial Scholarship", "Monash", "2017",
  "Monash Community Leaders Scholarship", "Monash", "2015 & 2016",
  "Mitcham Rotary Scholarship", "Rotary", "2011 & 2012"
) %>%
  brief_entries(scholarship, year, from)
```


## Competitions
```{r}
tribble(
  ~ competition, ~ from, ~ year,
  "UseR! 2018 Datathon Champion", "UseR!", "2018",
  "RMIT SBITL Analytics Competition Champion", "RMIT", "2017",
  "RMIT SBITL Analytics Competition Champion", "RMIT", "2016"
) %>%
  brief_entries(competition, year, from)
```
-->
